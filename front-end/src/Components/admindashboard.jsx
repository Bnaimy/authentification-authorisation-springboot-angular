import React, { Component } from "react";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import 'bootstrap/dist/css/bootstrap.css';
import '../authentification/css/style.css';
import { BrowserRouter as Router } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import '../authentification/css/style.css';
import Filliere from "./AdminComponents/Filliereops";
import Tab from 'react-bootstrap/Tab';
import Matiereops from './AdminComponents/Matiereops';
import Etudiantops from './AdminComponents/Etudiantops';
class Dashboard extends Component {
  state = {
    isOpen: false,
    filliere:false
  };
  logout = () => {
    localStorage.clear("authorization");
    this.props.history.push("/");
  }
  filliere = () => {
    this.setState({filliere:!this.state.filliere});
  }

  toggleCollapse = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  componentDidMount(props) {
    console.log("property_id", this.props.location.state.token_username);
  }

  render() {
    const filliere = this.state.filliere;
    if (filliere) {
      return <Filliere />
    }
    return (
      <Router>
        <Navbar collapseOnSelect expand="lg">
          <Navbar.Brand>EMSI SGDR</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
            </Nav>
            <Nav>
              <Nav.Link>{this.props.location.state.token_username}</Nav.Link>
              <Nav.Link eventKey={2} onClick={this.logout}>
                Log Out
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Tab.Container id="left-tabs-example" defaultActiveKey="0">
        <aside>
          <p> ADMIN DASHBOARD </p>
          <Nav.Item className="tabs">
            <Nav.Link eventKey="0" className="link"></Nav.Link>
          </Nav.Item>
          <Nav.Item className="tabs">
            <Nav.Link eventKey="1" className="link">Filliere Ops</Nav.Link>
          </Nav.Item>
          <Nav.Item className="tabs">
            <Nav.Link eventKey="2" className="link">Matieres Ops</Nav.Link>
          </Nav.Item>
          <Nav.Item className="tabs">
            <Nav.Link eventKey="3" className="link">Etudiants Ops</Nav.Link>
          </Nav.Item>
          <Nav.Item className="tabs">
            <Nav.Link eventKey="4" className="link">Salles Ops</Nav.Link>
          </Nav.Item>
          <Nav.Item className="tabs">
            <Nav.Link eventKey="5" className="link">Tables Ops</Nav.Link>
          </Nav.Item>
        </aside>
        <Tab.Content>
        <Tab.Pane eventKey="0">
        </Tab.Pane>
        <Tab.Pane eventKey="1">
          <Filliere />
        </Tab.Pane>
        <Tab.Pane eventKey="2">
          <Matiereops />
        </Tab.Pane>
        <Tab.Pane eventKey="3">
          <Etudiantops />
        </Tab.Pane>
        <Tab.Pane eventKey="4">
          <h1>4</h1>
        </Tab.Pane>
        <Tab.Pane eventKey="5">
          <h1>5</h1>
        </Tab.Pane>
      </Tab.Content>
      </Tab.Container>
      </Router>
    );
  }
}

export default Dashboard;