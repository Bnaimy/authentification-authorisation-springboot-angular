import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../../authentification/css/style.css';
import axios from "axios";
import ReactTable from "react-table";
import 'react-table/react-table.css';
import 'reactjs-popup/dist/index.css';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import swal from 'sweetalert';
class Filliere extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      users: [],
      loading: true,
      selected: null,
      open: false,
      open1: false,
      object: {
        id: '',
        description: '',
        nom_filliere: ''
      },
      objectupdate: {
        id: '',
        description: '',
        nom_filliere: ''
      }
    }
  }
  async getUsersData() {
    console.log(localStorage.getItem("authorization"));
    await axios.get('http://localhost:8080/api/v1/admin/consulter-fillieres', {
      headers: {
        'authorization': 'Bearer ' + localStorage.getItem("authorization")
      }}).then(res => {
        console.log(res.data)
        this.setState({ loading: false, users: res.data })
    }).catch((error) => {
  });
  }

componentDidMount() {
  this.getUsersData();
}

handlechange = (e) => {
  e.preventDefault();
  if (e.target.name === 'description') {
    // eslint-disable-next-line
    this.state.objectupdate.description = e.target.value;
    console.log(e.target.name);
  }
  if (e.target.name === 'nom_filliere') {
    // eslint-disable-next-line
    this.state.objectupdate.nom_filliere = e.target.value;
    console.log(e.target.name);
  }
  console.log(this.state.objectupdate)
}
handlechange1 = (e) => {
  e.preventDefault();
  if (e.target.name === 'description') {
    // eslint-disable-next-line
    this.state.objectupdate.description = e.target.value;
  }
  console.log(e.target.name);
  if (e.target.name === 'nom_filliere') {
    // eslint-disable-next-line
    this.state.objectupdate.nom_filliere = e.target.value;
  }
  console.log(this.state.objectupdate)
}
submitchanges = () => {
  const data = {nom_filliere: this.state.objectupdate.nom_filliere , description: this.state.objectupdate.description }
  console.log(data);
  axios({
    method: 'put',
    url: 'http://localhost:8080/api/v1/admin/modifier-filiere/'+this.state.object.id,
    data: {nom_filliere: this.state.objectupdate.nom_filliere , description: this.state.objectupdate.description },
    headers: {'authorization': 'Bearer ' + localStorage.getItem("authorization")}
    })
    .then(res => {
      swal({
        title: "success",
        text: "filliere modifier avec success",
        icon: "success",
        button: "Cancel",
      })
      window.location.reload(false);
    })
    .catch((error) => {
        if (error.response) {
          swal({
            title: "error",
            text: error.response.data,
            icon: "error",
            button: "Cancel",
          });
        } else if (error.request) {
          swal({
            title: "error",
            text: "something wrong",
            icon: "error",
            button: "Cancel",
          });
        }}
    );
}
submitchanges1 = () => {
  const data = {nom_filliere: this.state.objectupdate.nom_filliere , description: this.state.objectupdate.description }
  console.log(data);
  axios({
    method: 'post',
    url: 'http://localhost:8080/api/v1/admin/ajouter-filliere',
    data: {nom_filliere: this.state.objectupdate.nom_filliere , description: this.state.objectupdate.description },
    headers: {'authorization': 'Bearer ' + localStorage.getItem("authorization")}
    })
    .then(res => {
      swal({
        title: "success",
        text: "filliere ajouter avec success",
        icon: "success",
        button: "Cancel",
      })
      window.location.reload(false);
    })
    .catch((error) => {
        if (error.response) {
          swal({
            title: "error",
            text: error.response.data.message,
            icon: "error",
            button: "Cancel",
          });
        } else if (error.request) {
          swal({
            title: "error",
            text: "something wrong",
            icon: "error",
            button: "Cancel",
          });
        }}
    );
}
submitchanges2 = () => {
  axios({
    method: 'delete',
    url: 'http://localhost:8080/api/v1/admin/supprimer-filliere/'+this.state.object.id,
    headers: {'authorization': 'Bearer ' + localStorage.getItem("authorization")}
    })
    .then(res => {
      swal({
        title: "success",
        text: "filliere ajouter avec success",
        icon: "success",
        button: "Cancel",
      })
      window.location.reload(false);
    })
    .catch((error) => {
        if (error.response) {
          swal({
            title: "error",
            text: error.response.data.message,
            icon: "error",
            button: "Cancel",
          });
        } else if (error.request) {
          swal({
            title: "error",
            text: "something wrong",
            icon: "error",
            button: "Cancel",
          });
        }}
    );
}

render() {
  // eslint-disable-next-line
  const handleClickOpen = () => {
    this.setState({ open: true });
  };
  const handleClickOpen1 = () => {
    this.setState({ open1: true });
  };
  const handleClose = () => {
    this.setState({ open: false })
  };
  const handleClose1 = () => {
    this.setState({ open1: false })
  };
  const columns = [{
    Header: 'id',
    accessor: 'id',
  }
    , {
    Header: 'description',
    accessor: 'description',
  }

    , {
    Header: 'nomFilliere',
    accessor: 'nomFilliere',
  }
  ]
  return (
    <div style={{ position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)' }}>
      <center><h1>Liste Des Fillieres</h1></center>
      <br />
      <ReactTable
        data={this.state.users}
        columns={columns}
        pageSizeOptions={[5, 10]}
        defaultPageSize={5}
        getTrGroupProps={(state, rowInfo, column, instance) => {
          if (rowInfo !== undefined) {
            return {
              onClick: (e, handleOriginal) => {
                this.setState({
                  selected: rowInfo.original.id,
                  object:{
                    id:rowInfo.original.id,
                    nom_filliere:rowInfo.original.nomFilliere,
                    description:rowInfo.original.description
                  },
                  objectupdate:{
                    id:rowInfo.original.id,
                    nom_filliere:rowInfo.original.nomFilliere,
                    description:rowInfo.original.description
                  }
                })
                this.setState({ open: true });
              },
              style: {
                cursor: 'pointer',
                background: rowInfo.original.id === this.state.selectedIndex ? '#00afec' : 'transparent',
                color: rowInfo.original.id === this.state.selectedIndex ? 'white' : 'black'
              }
            }
          }
        }
        }
      />
      <button type="button" class="btn btn-outline-success" onClick={handleClickOpen1}>Ajouter Filliere</button>
      <Dialog open={this.state.open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Manipulation sur filliere</DialogTitle>
        <DialogContent>
          <DialogContentText>
            modifier ou supprimer une filliere
          </DialogContentText>
          <form>
            <TextField
              name="id"
              id="id"
              label={this.state.object.id}
              margin="normal"
              variant="outlined"
              value={this.state.object.id}
            />
            <br />
            <TextField
              name="nom_filliere"
              id="nom_filliere"
              label={this.state.object.nom_filliere}
              margin="normal"
              variant="outlined"
              onChange={this.handlechange}
            />
            <br />
            <TextField
              name="description"
              id="description"
              label={this.state.object.description}
              margin="normal"
              variant="outlined"
              onChange={this.handlechange}
            />
            <br />
            <button type="button" class="btn btn-outline-warning" onClick={this.submitchanges}>Modifier Filliere</button>
            <br />
            <button type="button" class="btn btn-outline-danger" onClick={this.submitchanges2}>Supprimer Filliere</button>
          </form>
        </DialogContent>
      </Dialog>

    <Dialog open={this.state.open1} onClose={handleClose1} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Manipulation sur filliere</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Ajouter Filliere
          </DialogContentText>
          <form>
            <TextField
              name="nom_filliere"
              id="nom_filliere"
              label="nom_filliere"
              margin="normal"
              variant="outlined"
              onChange={this.handlechange1}
              required
              noValidate
            />
            <br />
            <TextField
              name="description"
              id="description"
              label="description"
              margin="normal"
              variant="outlined"
              onChange={this.handlechange1}
              required
              noValidate
            />
            <br />
            <button type="button" class="btn btn-outline-warning" onClick={this.submitchanges1}>ajouter Filliere</button>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}
}

export default Filliere;