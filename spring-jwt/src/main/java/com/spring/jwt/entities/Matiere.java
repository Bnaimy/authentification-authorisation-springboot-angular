package com.spring.jwt.entities;
import java.io.Serializable;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
@Table(name="matiere")
public class Matiere implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")
	private int id;

	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="NOM_MATIERE")
	private String nomMatiere;

	//bi-directional many-to-one association to Filliere
	@ManyToOne
	@JoinColumn(name="filliereID")
	private Filliere filliere;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="iduser")
	private User user;

	public Matiere() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id2) {
		this.id = id2;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNomMatiere() {
		return this.nomMatiere;
	}

	public void setNomMatiere(String nomMatiere) {
		this.nomMatiere = nomMatiere;
	}

	public Filliere getFilliere() {
		return this.filliere;
	}

	public void setFilliere(Filliere filliere) {
		this.filliere = filliere;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}