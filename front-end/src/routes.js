import React from "react";
import {Route,BrowserRouter as Router,Switch} from "react-router-dom";
import Dashboard from "./Components/admindashboard";
import Auth from './authentification/auth';


const Routes = (props) => (
  <Router {...props}>
    <Switch>
      <Route path="/admindashboard" render={(props) => <Dashboard {...props}/>} />
      <Route path="/">
        <Auth />
      </Route>
    </Switch>
  </Router>
);
export default Routes;