package com.spring.jwt.entities;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="table_exam")
public class Table_Exam implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	//bi-directional many-to-one association to Salle
	@ManyToOne
	@JoinColumn(name="Salle")
	private Salle salle;

	public Table_Exam() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id2) {
		this.id = id2;
	}

	public Salle getSalle() {
		return this.salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

}