import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../../authentification/css/style.css';
import axios from "axios";
import ReactTable from "react-table";
import 'react-table/react-table.css';
import 'reactjs-popup/dist/index.css';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import swal from 'sweetalert';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

const useStyles = theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
});

class Matiereops extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            matieres: [],
            fillieres: [],
            loading: true,
            selected: null,
            open: false,
            open1: false,
            object: {
                id: '',
                description: '',
                nomMatiere: '',
                nomFilliere: '',
                nomUser: ''
            },
            objectupdate: {
                id: '',
                description: '',
                nomMatiere: '',
                nomFilliere: '',
                nomUser: '',
                idFilliere: ''
            }
        }
    }
    async getMatieresData() {
        console.log(localStorage.getItem("authorization"));
        await axios.get('http://localhost:8080/api/v1/admin/consulter-matieres', {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem("authorization")
            }
        }).then(res => {
            console.log(res.data)
            this.setState({ loading: false, matieres: res.data })
        }).catch((error) => {
        });
    }
    async getFillieresData() {
        await axios.get('http://localhost:8080/api/v1/admin/consulter-fillieres', {
            headers: {
                'authorization': 'Bearer ' + localStorage.getItem("authorization")
            }
        }).then(res => {
            console.log(res.data)
            this.setState({ loading: false, fillieres: res.data })
        }).catch((error) => {
        });
    }
    componentDidMount() {
        this.getMatieresData();
        this.getFillieresData();
    }
    handlechange = (e) => {
        e.preventDefault();
        if (e.target.name === 'description') {
            // eslint-disable-next-line
            this.state.objectupdate.description = e.target.value;
        }
        if (e.target.name === 'nomMatiere') {
            // eslint-disable-next-line
            this.state.objectupdate.nomMatiere = e.target.value;
        }
        if (e.target.name === 'nomFilliere') {
            // eslint-disable-next-line
            this.state.objectupdate.idFilliere = e.target.value;
        }
        console.log(this.state.objectupdate.idFilliere);
    }
    handlechange1 = (e) => {
        e.preventDefault();
        if (e.target.name === 'description') {
            // eslint-disable-next-line
            this.state.objectupdate.description = e.target.value;
        }
        if (e.target.name === 'nom_matiere') {
            // eslint-disable-next-line
            this.state.objectupdate.nomMatiere = e.target.value;
        }
        if (e.target.name === 'nomFilliere') {
            // eslint-disable-next-line
            this.state.objectupdate.idFilliere = e.target.value;
        }
        console.log(this.state.objectupdate)
      }
    submitchanges = () => {
        axios({
            method: 'put',
            url: 'http://localhost:8080/api/v1/admin/modifier-matiere/' + this.state.objectupdate.id,
            data: {description:this.state.objectupdate.description,nom_matiere:this.state.objectupdate.nomMatiere,id_filliere:this.state.objectupdate.idFilliere},
            headers: { 'authorization': 'Bearer ' + localStorage.getItem("authorization") }
        })
            .then(res => {
                swal({
                    title: "success",
                    text: "matiere modifier avec success",
                    icon: "success",
                    button: "Cancel",
                })
                window.location.reload(false);
            })
            .catch((error) => {
                if (error.response) {
                    swal({
                        title: "error",
                        text: error.response.data,
                        icon: "error",
                        button: "Cancel",
                    });
                } else if (error.request) {
                    swal({
                        title: "error",
                        text: "something wrong",
                        icon: "error",
                        button: "Cancel",
                    });
                }
            }
            );
    }
    submitchanges1 = () => {
        const data = { nom_filliere: this.state.objectupdate.nom_filliere, description: this.state.objectupdate.description }
        console.log(data);
        axios({
            method: 'post',
            url: 'http://localhost:8080/api/v1/admin/ajouter-matiere',
            data: {description:this.state.objectupdate.description,nom_matiere:this.state.objectupdate.nomMatiere,id_filliere:this.state.objectupdate.idFilliere},
            headers: { 'authorization': 'Bearer ' + localStorage.getItem("authorization") }
        })
            .then(res => {
                swal({
                    title: "success",
                    text: "matiere ajouter avec success",
                    icon: "success",
                    button: "Cancel",
                })
                window.location.reload(false);
            })
            .catch((error) => {
                if (error.response) {
                    swal({
                        title: "error",
                        text: error.response.data,
                        icon: "error",
                        button: "Cancel",
                    });
                } else if (error.request) {
                    swal({
                        title: "error",
                        text: "something wrong",
                        icon: "error",
                        button: "Cancel",
                    });
                }
            }
            );
    }
    submitchanges2 = () => {
        axios({
            method: 'delete',
            url: 'http://localhost:8080/api/v1/admin/supprimer-matiere/' + this.state.object.id,
            headers: { 'authorization': 'Bearer ' + localStorage.getItem("authorization") }
        })
            .then(res => {
                swal({
                    title: "success",
                    text: "matiere Supprimer avec success",
                    icon: "success",
                    button: "Cancel",
                })
                window.location.reload(false);
            })
            .catch((error) => {
                if (error.response) {
                    swal({
                        title: "error",
                        text: error.response.data,
                        icon: "error",
                        button: "Cancel",
                    });
                } else if (error.request) {
                    swal({
                        title: "error",
                        text: "something wrong",
                        icon: "error",
                        button: "Cancel",
                    });
                }
            }
            );
    }

    render() {
        const { classes } = this.props;
        // eslint-disable-next-line
        const handleClickOpen = () => {
            this.setState({ open: true });
        };
        const handleClickOpen1 = () => {
            this.setState({ open1: true });
        };
        const handleClose = () => {
            this.setState({ open: false })
        };
        const handleClose1 = () => {
            this.setState({ open1: false })
        };
        const columns = [{
            Header: 'id',
            accessor: 'id',
        },

        {
            Header: 'description',
            accessor: 'description',
        },

        {
            Header: 'nomMatiere',
            accessor: 'nomMatiere',
        },

        {
            Header: 'nomFilliere',
            accessor: 'nomFilliere',
        },

        {
            Header: 'nomAdmin',
            accessor: 'nomUser',
        }
        ]
        return (
            <div style={{ position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%, -50%)' }}>
                <center><h1>Liste Des Matieres(a rattraper)</h1></center>
                <br />
                <ReactTable
                    data={this.state.matieres}
                    columns={columns}
                    pageSizeOptions={[5, 10]}
                    defaultPageSize={5}
                    getTrGroupProps={(state, rowInfo, column, instance) => {
                        if (rowInfo !== undefined) {
                            return {
                                onClick: (e, handleOriginal) => {
                                    this.setState({
                                        selected: rowInfo.original.id,
                                        object: {
                                            id: rowInfo.original.id,
                                            description: rowInfo.original.description,
                                            nomMatiere: rowInfo.original.nomMatiere,
                                            nomFilliere: rowInfo.original.nomFilliere,
                                            nomUser: rowInfo.original.nomUser
                                        },
                                        objectupdate: {
                                            id: rowInfo.original.id,
                                            description: rowInfo.original.description,
                                            nomMatiere: rowInfo.original.nomMatiere,
                                            nomFilliere: rowInfo.original.nomFilliere,
                                            nomUser: rowInfo.original.nomUser
                                        }
                                    })
                                    this.setState({ open: true });
                                },
                                style: {
                                    cursor: 'pointer',
                                    background: rowInfo.original.id === this.state.selectedIndex ? '#00afec' : 'transparent',
                                    color: rowInfo.original.id === this.state.selectedIndex ? 'white' : 'black'
                                }
                            }
                        }
                    }
                    }
                />
                <button type="button" class="btn btn-outline-success" onClick={handleClickOpen1}>Ajouter Matiere</button>
                <Dialog open={this.state.open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Manipulation sur Matieres</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            modifier ou supprimer une Matiere
                        </DialogContentText>
                        <form>
                            <TextField
                                name="id"
                                id="id"
                                label="id"
                                margin="normal"
                                variant="outlined"
                                value={this.state.object.id}
                            />
                            <br />
                            <TextField
                                name="nomMatiere"
                                id="nomMatiere"
                                label={this.state.object.nomMatiere}
                                margin="normal"
                                variant="outlined"
                                onChange={this.handlechange}
                            />
                            <br />
                            <TextField
                                name="description"
                                id="description"
                                label={this.state.object.description}
                                margin="normal"
                                variant="outlined"
                                onChange={this.handlechange}
                            />
                            <TextField
                                name="nomFilliere1"
                                id="nomFilliere1"
                                label="nomFilliere"
                                value={this.state.object.nomFilliere}
                                margin="normal"
                                variant="outlined"
                            />
                            <br />
                            <FormControl className={classes.formControl}>
                                <InputLabel id="nomFilliere">Fillieres</InputLabel>
                                <Select
                                    name="nomFilliere"
                                    labelId="nomFilliere"
                                    id="nomFilliere"
                                    onChange={this.handlechange}
                                >
                                    {this.state.fillieres.map(nomFilliere => (
                                        <MenuItem key={nomFilliere.id} value={nomFilliere.id} >
                                            {nomFilliere.nomFilliere}
                                        </MenuItem>
                                    ))}
                                </Select>
                                <FormHelperText>Some important helper text</FormHelperText>
                            </FormControl>
                            <br />
                            <button type="button" class="btn btn-outline-warning" onClick={this.submitchanges}>Modifier Filliere</button>
                            <br />
                            <button type="button" class="btn btn-outline-danger" onClick={this.submitchanges2}>Supprimer Filliere</button>
                        </form>
                    </DialogContent>
                </Dialog>
                <Dialog open={this.state.open1} onClose={handleClose1} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Manipulation sur matiere</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Ajouter Matiere
                    </DialogContentText>
                        <form>
                        <TextField
                                name="description"
                                id="description"
                                label="description"
                                margin="normal"
                                variant="outlined"
                                onChange={this.handlechange1}
                            />
                            <br />
                            <TextField
                                name="nom_matiere"
                                id="nom_matiere"
                                label="nom_matiere"
                                margin="normal"
                                variant="outlined"
                                onChange={this.handlechange1}
                            />
                            <br />
                            <FormControl className={classes.formControl}>
                                <InputLabel id="nomFilliere">Fillieres</InputLabel>
                                <Select
                                    name="nomFilliere"
                                    labelId="nomFilliere"
                                    id="nomFilliere"
                                    onChange={this.handlechange1}
                                >
                                    {this.state.fillieres.map(nomFilliere => (
                                        <MenuItem key={nomFilliere.id} value={nomFilliere.id} >
                                            {nomFilliere.nomFilliere}
                                        </MenuItem>
                                    ))}
                                </Select>
                                <FormHelperText>Some important helper text</FormHelperText>
                            </FormControl>
                            <br />
                            <button type="button" class="btn btn-outline-warning" onClick={this.submitchanges1}>ajouter Matiere</button>
                        </form>
                    </DialogContent>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(useStyles)(Matiereops);