package com.spring.jwt.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="etage")
public class Etage implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")
	private int id;
	@ManyToOne
	@JoinColumn(name="UtilisateurID2")
	private User user;
	@OneToMany(mappedBy="etage")
	private List<Salle> salles;

	public Etage() {
	}
	
	public Etage(int id) {
		super();
		this.id = id;
	}

	public int getId2() {
		return this.id;
	}

	public void setId2(int id2) {
		this.id = id2;
	}


	public User getUtilisateur() {
		return user;
	}


	public void setUtilisateur(User utilisateur) {
		this.user = utilisateur;
	}


	public List<Salle> getSalles() {
		return salles;
	}


	public void setSalles(List<Salle> salles) {
		this.salles = salles;
	}

	
}