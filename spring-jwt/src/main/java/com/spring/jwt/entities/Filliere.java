package com.spring.jwt.entities;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the filliere database table.
 * 
 */
@Entity
@Table(name="filliere")
public class Filliere implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public Filliere() {};
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="NOM_FILLIERE")
	private String nomFilliere;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="UtilisateurID2")
	private User user;

	//bi-directional many-to-one association to Matiere
	@OneToMany(mappedBy="filliere")
	private List<Matiere> matieres;
	public Filliere(String description, String nomFilliere, User user) {
		this.description = description;
		this.nomFilliere = nomFilliere;
		this.user = user;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}



	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNomFilliere() {
		return this.nomFilliere;
	}

	public void setNomFilliere(String nomFilliere) {
		this.nomFilliere = nomFilliere;
	}

	/*
	public List<Matiere> getMatieres() {
		return this.matieres;
	}

	public void setMatieres(List<Matiere> matieres) {
		this.matieres = matieres;
	}

	public Matiere addMatiere(Matiere matiere) {
		getMatieres().add(matiere);
		matiere.setFilliere(this);

		return matiere;
	}

	public Matiere removeMatiere(Matiere matiere) {
		getMatieres().remove(matiere);
		matiere.setFilliere(null);

		return matiere;
	}
	*/

}