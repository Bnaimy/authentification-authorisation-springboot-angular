package com.spring.jwt.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import com.spring.jwt.entities.ConfirmationToken;

public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, String> {
    ConfirmationToken findByConfirmationToken(String confirmationToken);
}