package com.spring.jwt.services;
import java.util.List;
import java.util.Optional;
import org.apache.catalina.security.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import com.spring.jwt.DTO.FilliereRequestDto;
import com.spring.jwt.Exceptions.FilliereExistException;
import com.spring.jwt.entities.Filliere;
import com.spring.jwt.entities.User;
import com.spring.jwt.repositories.FilliereRepository;

import javassist.NotFoundException;

@Import(SecurityConfig.class)
@Service
@Component
public class AdminService {

	@Autowired
	FilliereRepository filliererepo ;
	
	public void ajouter(FilliereRequestDto filliere) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User u = (User)auth.getPrincipal();
		System.out.println(filliere.getNom_filliere());
		Filliere f = new Filliere(filliere.getDescription(),filliere.getNom_filliere(),u);
		if (filliererepo.findByNomFilliere(f.getNomFilliere()).isPresent()) {
			throw new FilliereExistException("filliere deja ajouter par monsieur"+u.getUsername());	
		}
		filliererepo.save(f);
	}

	public void modifier(FilliereRequestDto filiere,@RequestParam Long id) throws NotFoundException {
		Optional<Filliere> filiereU = filliererepo.findById(id);
		Filliere F = new Filliere();
		if (filiereU.isPresent()) {
			
			F = filiereU.get();
			
		}
		else 
		{
			throw new NotFoundException("not found");
		}
		F.setNomFilliere(filiere.getNom_filliere());
		F.setDescription(filiere.getDescription());
		
		filliererepo.save(F);	
	}

	public void supprimer(Long id) throws NotFoundException {
		Optional<Filliere> filiereU = filliererepo.findById(id);
		Filliere F = new Filliere();
		if (filiereU.isPresent()) {
			
			F = filiereU.get();
		}
		else 
		{
			throw new NotFoundException("not found");
		}
		filliererepo.deleteById(F.getId());	
	}

	public List<Filliere> consulterfilliere() throws NotFoundException {
		List<Filliere> list = filliererepo.findAll();
		if (list.isEmpty()) {
			throw new NotFoundException("not found");
		}
		return list;
	}

	public Filliere consulterfillierebyid(@PathVariable("id") Long id) throws NotFoundException {
		Optional<Filliere> f = filliererepo.findById(id);
		Filliere filliere = new Filliere();
		if (f.isPresent()) {
			filliere = f.get();
		}
		else {
			throw new NotFoundException("not found");
		}
		return filliere;
	}

	public void supprimerTout(Long[] ids) {
		for (Long long1 : ids) {
			filliererepo.deleteById(long1);
		}
	}

}