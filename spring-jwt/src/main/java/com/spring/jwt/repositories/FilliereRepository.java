package com.spring.jwt.repositories;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.spring.jwt.entities.Filliere;

public interface FilliereRepository extends JpaRepository<Filliere, Long>  {
	
	Optional<Filliere> findByNomFilliere(String nomFilliere);

}
