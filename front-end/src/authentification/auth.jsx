import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {TabContent,TabPane,Nav,NavItem,NavLink} from 'reactstrap';
import classnames from 'classnames';
import Signin from './login';
import Signup from './signup' ;
import 'bootstrap/dist/css/bootstrap.css';
import './css/style.css' ;
class Auth extends React.Component {

    constructor(props){
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab : '1'
        };
    }
    toggle(tab) {
        if(this.state.activeTab !== tab){
            this.setState({activeTab:tab});
        }
    }
    render() {

        return (
            <div>
                <Nav tabs fill className="nav-fill flex-column flex-md-row nav" pills role="tablist">
                <NavItem style={{backgroundcolor:"black"}}>
                    <NavLink className={classnames({ active: this.state.activeTab === '1'})} onClick={() => { this.toggle('1'); }} style={{color:"black"}}>
                        Sign In
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.toggle('2'); }} style={{color:"black"}}> 
                        Sign Up
                    </NavLink>
                </NavItem>
                </Nav>
            <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                    <Signin />
                </TabPane>
                <TabPane tabId="2">
                    <Signup />
                  </TabPane>
            </TabContent>
            </div>
        );
    }

}
export default Auth;