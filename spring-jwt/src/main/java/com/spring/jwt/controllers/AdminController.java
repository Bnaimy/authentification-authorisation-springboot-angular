package com.spring.jwt.controllers;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.spring.jwt.DTO.FilliereRequestDto;
import com.spring.jwt.Exceptions.FilliereExistException;
import com.spring.jwt.services.AdminService;
import com.spring.jwt.entities.Filliere;
import javassist.NotFoundException;

@RequestMapping("/api/v1/admin")
@RestController
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {
	
	@Autowired
	AdminService adminservice ;
	
    @GetMapping("/")
    public String testAdmin(){
        return "HELLO ADMIN";
    }
    
    @GetMapping("consulter-fillieres")
    public ResponseEntity<List<Filliere>> consulterfilliere(){
    	List<Filliere> L ;
    	try {
    		L = adminservice.consulterfilliere();
    	}catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    	}
    	return new ResponseEntity<>(L,HttpStatus.OK);
}
    @GetMapping("consulter-fillieres/{id}")
    public ResponseEntity<Filliere> consulterfillierebyid(@PathVariable("id") Long id){
    	Filliere F = new Filliere();
    	try {
    	   F = adminservice.consulterfillierebyid(id);
		} catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    	
    	return new ResponseEntity<>(F,HttpStatus.OK);
    }
    @PostMapping("/ajouter-filliere")
    public ResponseEntity<Void> ajouter(@RequestBody @Validated FilliereRequestDto filliere)
    {
    	try {
    		adminservice.ajouter(filliere);
		} catch (FilliereExistException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.FOUND);
		}
    	return new ResponseEntity<>(HttpStatus.OK);
    }
    @PutMapping("/modifier-filiere/{id}")
    public ResponseEntity<Void> modifier(@RequestBody @Validated FilliereRequestDto filiere,@PathVariable("id") Long id)
    
    {
    	try {
    		adminservice.modifier(filiere,id);
		} catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    	return new ResponseEntity<>(HttpStatus.OK);
    	}
    
    @DeleteMapping("/supprimer-filliere/{id}")
	public ResponseEntity<Void> supprimer(@PathVariable("id") Long id)
	{
		try {
			adminservice.supprimer(id);
		} catch (NotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @DeleteMapping("/supprimer-filliere-tout/{ids}")
	public ResponseEntity<Void> supprimerTout(@PathVariable Long[] ids)
	{
			adminservice.supprimerTout(ids);
		    return new ResponseEntity<>(HttpStatus.OK);
    }

		}
	