package com.spring.jwt.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;
@Entity
@Table(name="salle")
public class Salle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="EtageID")
	private User etage;

	//bi-directional many-to-one association to Table
	@OneToMany(mappedBy="salle")
	private List<Table_Exam> tables;

	public Salle() {
	}
	public Salle(int id2, User etage, List<Table_Exam> tables) {
		super();
		this.id = id2;
		this.etage = etage;
		this.tables = tables;
	}

	public int getId2() {
		return this.id;
	}

	public void setId2(int id2) {
		this.id = id2;
	}

	public User getEtage() {
		return this.etage;
	}

	public void setEtage(User etage) {
		this.etage = etage;
	}

	public List<Table_Exam> getTables() {
		return this.tables;
	}

	public void setTables(List<Table_Exam> tables) {
		this.tables = tables;
	}

}