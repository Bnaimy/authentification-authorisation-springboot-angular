package com.spring.jwt.DTO;

public class FilliereRequestDto {
	
	
	private String nom_filliere ;
	private String Description ;
	
	public FilliereRequestDto() {};
	
	public FilliereRequestDto(String nom_filliere, String description) {
		this.nom_filliere = nom_filliere;
		Description = description;
	}

	public String getNom_filliere() {
		return nom_filliere;
	}

	public void setNom_filliere(String nom_filliere) {
		this.nom_filliere = nom_filliere;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}
	
}
