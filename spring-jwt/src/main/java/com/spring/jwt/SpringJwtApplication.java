package com.spring.jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.spring.jwt"})
@EnableJpaRepositories(basePackages="com.spring.jwt.repositories")
@EnableTransactionManagement
@EntityScan(basePackages="com.spring.jwt.entities")
@SpringBootApplication(scanBasePackages={"com.spring.jwt"})
public class SpringJwtApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringJwtApplication.class, args);
		
	}

}
